<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Sign-in</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="style.css" />
</head>
<body>
    <?php
        $email = explode("@", $_POST["email"], 2);
    ?>

    <section class="success body">
        <h1>Olá, <?= ucfirst($email[0]) ?>! <br>Seja bem vindo(a) :)</h1>
    </section>
</body>
</html>